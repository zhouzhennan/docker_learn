docker container lerning
<!-- 开启容器的目录 -->
export JINCHUAN_2D_DET_PROJECT_DIR=[path to your project dir]
<!-- 设置容器的名字 -->
export JINCHUAN_2D_DET_CONTAINER_NAME=[your_container_name]
<!-- 退出容器 -->
exit
<!-- 进入容器 -->
docker container attach <ID>
<!-- 在运行的容器中执行命令 -->
docker container exec -it your name bash
